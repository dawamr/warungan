<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function() {
    Route::get('/', 'ProfileController@index')->name('profile');
    Route::put('/', 'ProfileController@update')->name('profile.update');
});

Route::post('/', 'MainController@getDetailProduct')->name('get_detail_product');
