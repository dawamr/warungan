<?php

Route::get('/', 'DashboardController@index')->name('admin.dashboard');

Route::group(['prefix' => 'products', 'middleware' => 'role:admin,product.view'], function () {
    Route::get('/', 'ProductController@index')->name('admin.product');
    Route::get('/getproducts', 'ProductController@getProducts')->name('admin.product.getproducts');

    Route::post('/', 'ProductController@store')->name('admin.product.store');
    Route::post('/edit', 'ProductController@edit')->name('admin.product.edit');
    Route::put('/', 'ProductController@update')->name('admin.product.update');
    Route::delete('/', 'ProductController@destroy')->name('admin.product.destroy');
});

Route::group(['prefix' => 'users', 'middleware' => 'role:admin,user.view'], function () {
    Route::get('/', 'UserController@index')->name('admin.user');
    Route::get('/getusers', 'UserController@getUsers')->name('admin.user.getusers');

    Route::post('/', 'UserController@store')->name('admin.user.store');
    Route::post('/edit', 'UserController@edit')->name('admin.user.edit');
    Route::put('/', 'UserController@update')->name('admin.user.update');
    Route::delete('/', 'UserController@destroy')->name('admin.user.destroy');
});

Route::group(['prefix' => 'history', 'middleware' => 'role:admin,product_view.view'], function () {
    Route::get('/', 'ProductViewController@index')->name('admin.product_view');
    Route::get('/getHistory', 'ProductViewController@getHistory')->name('admin.product_view.gethistory');
});

Route::group(['prefix' => 'settings', 'middleware' => 'role:admin,setting.view'], function () {
    Route::get('/', 'SettingController@index')->name('admin.setting');
    Route::put('/', 'SettingController@update')->name('admin.setting.update');
});

Route::group(['prefix' => 'permissions', 'middleware' => 'role:admin,permission.view'], function () {
    Route::get('/', 'PermissionController@index')->name('admin.permission');
    Route::get('/getpermissions', 'PermissionController@getPermissions')->name('admin.permission.getpermissions');

    Route::post('/', 'PermissionController@store')->name('admin.permission.store');
    Route::post('/edit', 'PermissionController@edit')->name('admin.permission.edit');
    Route::put('/', 'PermissionController@update')->name('admin.permission.update');
    Route::delete('/', 'PermissionController@destroy')->name('admin.permission.destroy');
});

Route::group(['prefix' => 'roles', 'middleware' => 'role:admin,role.view'], function () {
    Route::get('/', 'RoleController@index')->name('admin.role');
    Route::get('/getroles', 'RoleController@getRoles')->name('admin.role.getroles');

    Route::post('/', 'RoleController@store')->name('admin.role.store');
    Route::post('/edit', 'RoleController@edit')->name('admin.role.edit');
    Route::put('/', 'RoleController@update')->name('admin.role.update');
    Route::delete('/', 'RoleController@destroy')->name('admin.role.destroy');
    Route::post('/manage/get', 'RoleController@getManage')->name('admin.role.manage.get');
    Route::put('/manage', 'RoleController@manage')->name('admin.role.manage');
});
