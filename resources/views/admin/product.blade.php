@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Produk</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("admin.dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">Produk</div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Product</h4>
                    @can ('product.create')
                        <div class="card-header-action">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-product').modal('show');" tooltip="Tambah Produk"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="product-list">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">
                                        #
                                    </th>
                                    <th>Nama</th>
                                    <th>Harga Normal</th>
                                    <th>Harga Agen</th>
                                    <th>Rak</th>
                                    <th>Kode</th>
                                    <th>Gambar</th>
                                    <th width="150">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@can ('product.create')
    <div class="modal fade" tabindex="-1" product="dialog" id="modal-add-product">
        <div class="modal-dialog modal-dialog-centered modal-lg" product="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-add-product" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="code" id="add-product-code" value="{{ Date('zhis') }}" readonly required autofocus>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" id="add-product-name" required autofocus>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Harga Normal</label>
                                <input type="text" class="form-control" name="price" id="add-product-price" required autofocus>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Harga Agen</label>
                                <input type="text" class="form-control" name="price_subcribe" id="add-product-price-subcribe" required autofocus>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Rak</label>
                                <input type="text" class="form-control" name="rak" id="add-product-rak" autofocus>
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Gambar <sup>(Opsional)</sup></label>
                                <div id="add-picture-preview" class="image-preview">
                                    <label for="add-picture-upload" id="add-picture-label">Pilih Gambar</label>
                                    <input type="file" name="picture" id="add-picture-upload" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-add-product">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('product.update')
<div class="modal fade" tabindex="-1" product="dialog" id="modal-update-product">
    <div class="modal-dialog modal-dialog-centered modal-lg" product="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="javascript:void(0)" id="form-update-product" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="" id="update-product-id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Kode</label>
                            <input type="text" class="form-control" name="code" id="update-product-code" readonly required autofocus>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" id="update-product-name" required autofocus>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Harga Normal</label>
                            <input type="text" class="form-control" name="price" id="update-product-price" required autofocus>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Harga Agen</label>
                            <input type="text" class="form-control" name="price_subcribe" id="update-product-price-subcribe" required autofocus>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Rak</label>
                            <input type="text" class="form-control" name="rak" id="update-product-rak" autofocus>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Gambar <sup>(Opsional)</sup></label>
                            <div id="update-picture-preview" class="image-preview">
                                <label for="update-picture-upload" id="update-picture-label">Pilih Gambar</label>
                                <input type="file" name="picture" id="update-picture-upload" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" id="btn-update-product">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endcan
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/simple.money.format.js') }}"></script>
@endsection

@section('js-script')
<script>

$(function () {
    "use strict";

    getProductList();
    imagePreview();

    $('#update-product-price').simpleMoneyFormat()
    $('#add-product-price').simpleMoneyFormat()
    $('#update-product-price-subcribe').simpleMoneyFormat()
    $('#add-product-price-subcribe').simpleMoneyFormat()
    $('#add-product-price').keyup(()=>{
        $('#add-product-price-subcribe').val($('#add-product-price').val());
    });

    var myVar = setInterval(myTimer, 3000);
    var d = new Date();
    let dayOfYear = "<?= Date('z'); ?>"

    function myTimer() {
        var d = new Date();
        $("#add-product-code").val(dayOfYear + d.getHours() + d.getMinutes() + d.getSeconds())
    }

    @can ('product.create')
    $("#form-add-product").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        addProduct(formData);
    });
    @endcan

    @can ('product.update')
    $("#form-update-product").on("submit", function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        updateProduct(formData);
    });
    @endcan
});

async function getProductList()
{
    $("#product-list").dataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.product.getproducts') }}",
        destroy: true,
        columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'price' },
            { data: 'price_subcribe' },
            { data: 'rak' },
            { data: 'code' },
            { data: 'picture' },
            { data: 'action' },
        ]
    });
}

async function imagePreview()
{
    $.uploadPreview({
        input_field: "#add-picture-upload",   // Default: .image-upload
        preview_box: "#add-picture-preview",  // Default: .image-preview
        label_field: "#add-picture-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
    });
    $.uploadPreview({
        input_field: "#update-picture-upload",   // Default: .image-upload
        preview_box: "#update-picture-preview",  // Default: .image-preview
        label_field: "#update-picture-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false,                // Default: false
        success_callback: null          // Default: null
    });
}

@can ('product.create')
async function addProduct(formData)
{

    $.ajax({
        url: "{{ route('admin.product.store') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend() {
            $("#btn-add-product").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-add-product").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-add-product").removeAttr('disabled', 'disabled');
            $("#btn-add-product").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-add-product")[0].reset();
                $('#add-picture-preview').css('background', 'transparent');
                $('#modal-add-product').modal('hide');
                getProductList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can ('user.update')
async function editProduct(obj)
{
    var id = $(obj).data('id');
    $('#modal-update-product').modal('show');
    $('#form-update-product')[0].reset();
    $('#update-picture-preview').css('background', 'transparent');

    $.ajax({
        url: "{{ route('admin.product.edit') }}",
        type: "POST",
        dataType: "json",
        data: {
            "id": id,
            "_method": "POST",
            "_token": "{{ csrf_token() }}"
        },
        beforeSend() {
            $("#btn-update-product").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-update-product").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-product").removeAttr('disabled', 'disabled');
            $("#btn-update-product").removeClass('btn-progress');
        },
        success : function(result) {
            $('#update-product-id').val(result['data']['id']);
            $('#update-product-code').val(result['data']['code']);
            $('#update-product-name').val(result['data']['name']);
            $('#update-product-price').val(result['data']['price']);
            $('#update-product-price-subcribe').val(result['data']['price_subcribe']);
            $('#update-product-rak').val(result['data']['rak']);
            $('#update-picture-preview').css('background', `url('{{ asset('`+result[`data`][`picture`]+`') }}') center center / cover transparent`);
        }
    });
}

async function updateProduct(formData)
{
    $.ajax({
        url: "{{ route('admin.product.update') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend() {
            $("#btn-update-product").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-update-product").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-product").removeAttr('disabled', 'disabled');
            $("#btn-update-product").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-update-product")[0].reset();
                $('#modal-update-product').modal('hide');
                getProductList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can('product.delete')
async function deleteProduct(object)
{
    var id = $(object).data('id');
    swal({
        title: 'Anda yakin?',
        text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            swal({
                title: 'Loading..!',
                text: 'Tunggu sebentar..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                buttons:false,
            })
            $.ajax({
                url: "{{ route('admin.product.destroy') }}",
                type: "POST",
                dataType: "json",
                data: {
                    "id": id,
                    "_method": "DELETE",
                    "_token": "{{ csrf_token() }}"
                },
                beforeSend() {

                },
                complete() {
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        getProductList();
                    }
                    swalNotification(result['status'], result['msg']);
                }
            });
        } else {
            swal('Data Anda Aman!', {icon:'success'});
        }
    });

}
@endcan

async function swalNotification(status, message)
{
    swal(message, {
        icon: status,
    });
}

async function notification(status, titleToast, messageToast)
{
    if(status == 'success'){
        iziToast.success({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'warning'){
        iziToast.warning({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'error'){
        iziToast.error({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }

}

</script>
@endsection
