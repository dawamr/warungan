@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Riwayat</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("admin.dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">Riwayat</div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Riwayat</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="history-list">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">
                                        #
                                    </th>
                                    <th>Nama</th>
                                    <th>Terakhir Dilihat</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
<script>

$(function () {
    "use strict";

    getHistoryList();

});

async function getHistoryList()
{
    $("#history-list").dataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.product_view.gethistory') }}",
        destroy: true,
        columns: [
            { data: 'id' },
            { data: 'product_name' },
            { data: 'created_at' },
        ],
        order: [
            [ 0, 'desc' ]
        ]
    });
}

</script>
@endsection

