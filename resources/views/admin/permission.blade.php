@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Permission</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("admin.dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">Permission</div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Permission</h4>
                    @can ('permission.create')
                        <div class="card-header-action">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-permission').modal('show');" tooltip="Tambah Permission"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="permission-list">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">
                                        #
                                    </th>
                                    <th>Nama</th>
                                    <th width="150">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@can ('permission.create')
    <div class="modal fade" tabindex="-1" permission="dialog" id="modal-add-permission">
        <div class="modal-dialog modal-dialog-centered" permission="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Permission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-add-permission">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" id="add-permission-name" required autofocus>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-add-permission">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('permission.update')
<div class="modal fade" tabindex="-1" permission="dialog" id="modal-update-permission">
    <div class="modal-dialog modal-dialog-centered" permission="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Permission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="javascript:void(0)" id="form-update-permission">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="" id="update-permission-id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="name" id="update-permission-name" required autofocus>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" id="btn-update-permission">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endcan
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
<script>

$(function () {
    "use strict";

    getPermissionList();

    @can ('permission.create')
    $("#form-add-permission").on("submit", function(e) {
        e.preventDefault();
        addPermission();
    });
    @endcan

    @can ('permission.update')
    $("#form-update-permission").on("submit", function(e) {
        e.preventDefault();
        updatePermission();
    });
    @endcan
});

async function getPermissionList()
{
    $("#permission-list").dataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.permission.getpermissions') }}",
        destroy: true,
        columns: [
            { data: 'id' },
            { data: 'name' },
            { data: 'action' },
        ]
    });
}

@can ('permission.create')
async function addPermission()
{
    var formData = $("#form-add-permission").serialize();

    $.ajax({
        url: "{{ route('admin.permission.store') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("#btn-add-permission").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-add-permission").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-add-permission").removeAttr('disabled', 'disabled');
            $("#btn-add-permission").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-add-permission")[0].reset();
                $('#modal-add-permission').modal('hide');
                getPermissionList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can ('user.update')
async function editPermission(obj)
{
    var id = $(obj).data('id');
    $('#modal-update-permission').modal('show');
    $('#form-update-permission')[0].reset();

    $.ajax({
        url: "{{ route('admin.permission.edit') }}",
        type: "POST",
        dataType: "json",
        data: {
            "id": id,
            "_method": "POST",
            "_token": "{{ csrf_token() }}"
        },
        beforeSend() {
            $("#btn-update-permission").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-update-permission").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-permission").removeAttr('disabled', 'disabled');
            $("#btn-update-permission").removeClass('btn-progress');
        },
        success : function(result) {
            $('#update-permission-id').val(result['data']['id']);
            $('#update-permission-name').val(result['data']['name']);
        }
    });
}

async function updatePermission()
{
    var formData = $("#form-update-permission").serialize();

    $.ajax({
        url: "{{ route('admin.permission.update') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("#btn-update-permission").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-update-permission").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-permission").removeAttr('disabled', 'disabled');
            $("#btn-update-permission").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-update-permission")[0].reset();
                $('#modal-update-permission').modal('hide');
                getPermissionList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can('permission.delete')
async function deletePermission(object)
{
    var id = $(object).data('id');
    swal({
        title: 'Anda yakin?',
        text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            swal({
                title: 'Loading..!',
                text: 'Tunggu sebentar..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                buttons:false,
            })
            $.ajax({
                url: "{{ route('admin.permission.destroy') }}",
                type: "POST",
                dataType: "json",
                data: {
                    "id": id,
                    "_method": "DELETE",
                    "_token": "{{ csrf_token() }}"
                },
                beforeSend() {

                },
                complete() {
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        getPermissionList();
                    }
                    swalNotification(result['status'], result['msg']);
                }
            });
        } else {
            swal('Data Anda Aman!', {icon:'success'});
        }
    });

}
@endcan

async function swalNotification(status, message)
{
    swal(message, {
        icon: status,
    });
}

async function notification(status, titleToast, messageToast)
{
    if(status == 'success'){
        iziToast.success({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'warning'){
        iziToast.warning({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'error'){
        iziToast.error({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }

}

</script>
@endsection
