<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
<title>{{ $app_name }}</title>
<link rel="shortcut icon" href="{{ asset($app_logo) }}" type="image/x-icon">
<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
