<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.dashboard') }}">{{ $app_name }}</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard') }}">MS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Menu</li>
            <li><a href="{{ route("admin.dashboard") }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
            @can('product.view')
                <li><a href="{{ route("admin.product") }}" class="nav-link"><i class="fas fa-cubes"></i><span>Produk</span></a></li>
            @endcan
            @can('user.view')
                <li><a href="{{ route("admin.user") }}" class="nav-link"><i class="fas fa-users"></i><span>User</span></a></li>
            @endcan
            @can('product_view.view')
                <li><a href="{{ route("admin.product_view") }}" class="nav-link"><i class="fas fa-history"></i><span>Riwayat</span></a></li>
            @endcan
            <li><a href="{{ route("profile") }}" class="nav-link"><i class="far fa-user"></i><span>Profil</span></a></li>
            @if(auth()->user()->can('setting.view') || auth()->user()->can('permission.view') || auth()->user()->can('role.view'))
                <li class="dropdown">
                    <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cog"></i> <span>Pengaturan</span></a>
                    <ul class="dropdown-menu">
                        @can('setting.view')<li><a class="nav-link" href="{{ route("admin.setting") }}">Pengaturan Umum</a></li>@endcan
                        @can('permission.view')<li><a class="nav-link" href="{{ route("admin.permission") }}">Permission</a></li>@endcan
                        @can('role.view')<li><a class="nav-link" href="{{ route("admin.role") }}">Role</a></li>@endcan
                    </ul>
                </li>
            @endif
        </ul>
    </aside>
</div>
