<nav class="navbar navbar-expand-lg main-navbar">
    <div class="container">
        <a href="{{ route('main') }}" class="navbar-brand sidebar-gone-hide">{{ $app_name }}</a>
    </div>
</nav>
