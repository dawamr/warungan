<!DOCTYPE html>
<html lang="id">
    <head>
        @include('templates._partials._head')
        @include('templates._partials._styles')
    </head>
    <body class="layout-3" style="overflow-y: scroll">
        <div id="app">
            <div class="main-wrapper container">
                <div class="navbar-bg" style="max-height:70px;"></div>
                @include('templates._partials._navbar_front')
                <!-- Main Content -->
				<div class="main-content" style="padding-top:90px;">
					<section class="section">
                        @yield('content')
                    </section>
                    @include('templates.components.modals')
                </div>
                @include('templates._partials._footer')
            </div>
        </div>
        @include('templates._partials._scripts')
    </body>
</html>
