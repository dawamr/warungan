@extends('templates.auth')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">

                <div class="card card-primary">
                    <div class="card-header"><h4>Login</h4></div>

                    <div class="card-body">
                        <form method="POST" action="javascript:void(0)" class="needs-validation" novalidate="" id="form-login">
                            @csrf
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('email') }}" tabindex="1" required autofocus>
                                <div class="invalid-feedback">
                                    Silakan isi username Anda
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                                <div class="invalid-feedback">
                                    Silakan isi password Anda
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                    <label class="custom-control-label" for="remember-me">Ingat Saya</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4" id="btn-login">
                                    Login
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="simple-footer">
                    {{ date('Y') }} &copy; {{ $app_name }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
@endsection

@section('js-script')
    <script>

        $(document).ready(function () {
            $("#form-login").on("submit", function(e) {
                e.preventDefault();

                if($("#username").val().length == 0 || $("#password").val().length == 0){
                    return false;
                }

                login();
            });
        });

        async function login()
        {
            var formData = $("#form-login").serialize();

            $.ajax({
                url: "{{ route('login') }}",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("#btn-login").addClass('btn-progress');
                    $("input").attr('disabled', 'disabled');
                    $("#btn-login").attr('disabled', 'disabled');
                },
                complete() {
                    $("input").removeAttr('disabled', 'disabled');
                    $("#btn-login").removeAttr('disabled', 'disabled');
                    $("#btn-login").removeClass('btn-progress');
                },
                success : function(result) {
                    notification(result['status'], result['title'], result['msg']);

                    $("#username").focus();

                    if(result['status'] == 'success'){
                        $("#form-login")[0].reset();
                        window.location="{{ route('admin.dashboard') }}";
                    }
                }
            });
        }

        async function notification(status, titleToast, messageToast)
        {
            if(status == 'success'){
                iziToast.success({
                    title: titleToast,
                    message: messageToast,
                    position: 'topRight'
                });
            }
            if(status == 'error'){
                iziToast.error({
                    title: titleToast,
                    message: messageToast,
                    position: 'topRight'
                });
            }

        }

    </script>
@endsection

