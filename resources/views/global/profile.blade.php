@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Profil</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Profil</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">Hai, {{ auth()->user()->name }}!</h2>
    <p class="section-lead">
        Ubah informasi tentang diri Anda di halaman ini.
    </p>

    <div class="row mt-sm-4">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <form method="post" class="needs-validation" novalidate="" action="javascript:void(0)" id="form-update-profile">
                    <div class="card-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @csrf
                            @method('PUT')
                            <div class="form-group col-md-6 col-12">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" value="{{ auth()->user()->username }}" required="">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" value="{{ auth()->user()->name }}" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-12">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>Password (Opsional)</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-primary" type="submit" id="btn-update-profile">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
@endsection

@section('js-script')
<script>

$(function() {
    $("#form-update-profile").on("submit", function(e) {
        e.preventDefault();
        updateProfile();
    });
});

async function updateProfile()
{
    var formData = $("#form-update-profile").serialize();

    $.ajax({
        url: "{{ route('profile.update') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("input").attr('disabled', 'disabled');
            $("#btn-update-profile").addClass('btn-progress');

        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("#btn-update-profile").removeClass('btn-progress');
        },
        success : function(result) {
            $('input[type="password"]').val('');

            notification(result['status'], result['title'], result['msg']);
        }
    });
}

async function notification(status, titleToast, messageToast)
{
    if(status == 'success'){
        iziToast.success({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'warning'){
        iziToast.warning({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'error'){
        iziToast.error({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }

}

</script>
@endsection
