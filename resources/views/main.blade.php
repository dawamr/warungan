
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>{{ $app_name }}</title>
  <link rel="shortcut icon" href="{{ asset($app_logo) }}" type="image/x-icon">
  <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/select2.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/tooltip.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/pages/main-loading.css') }}">
    <style>
        .no-gutter {
            margin-right: 0;
            margin-left: 0;
        }

        .no-gutter > [class*="col-"] {
            padding-right: 0;
            padding-left: 0;
        }
        .select2-container {
            width: 100% !important;
        }
    </style>
    <script>
        function retrive(list, price){
            let totHarga = sessionStorage.getItem('total_harga')
            totHarga = parseInt(totHarga) - parseInt(price);
            sessionStorage.setItem('total_harga', totHarga)  
            $('#total_price').html(sessionStorage.getItem('total_harga')).simpleMoneyFormat();
            $('#list'+ list).hide()
        }
    </script>
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="margin-top: 1.5em">
        <a href="{{ route('main') }}" class="navbar-brand sidebar-gone-hide">{{ $app_name }}</a>
        <form class="form-inline ml-auto">
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            @if(\Auth::user())
            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            @endif
            <div onclick="location.href= '/login'" class="d-sm-none d-lg-inline-block">@if(\Auth::user()) {{ \Auth::user()->name }} @else Masuk @endif</div></a>
            @if(\Auth::user())
            <div class="dropdown-menu dropdown-menu-right">
                <a href="#" class="dropdown-item has-icon text-danger">
                  <i class="fas fa-sign-out-alt"></i> Logout
                </a>
              </div>
            @endif
          </li>
        </ul>
      </nav>

      {{-- <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a href="{{ url('login') }}" class="nav-link"><i class="far fa-heart"></i><span>Login</span></a>
            </li>
          </ul>
        </div>
      </nav> --}}

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header" id="header-toko">
            <h1 class="text-center">{{ $app_name }}</h1>
          </div>

          <div class="section-body">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="card card-primary">
                      <div class="card-header">
                        <h4>Pencarian Barang</h4>
                      </div>
                      <div class="card-body">
                        <form action="javascript:void(0)" method="post" id="form-get-detail-product">
                            @csrf
                            <div class="row no-gutter">
                                <div class="col-md-3">
                                    <label for="Type Pembeli"> Tipe Pembeli</label>
                                    <select name="type_price" class="form-control" id="type_price">
                                        <option  selected value="biasa">Biasa</option>
                                        <option value="agen">Agen</option>
                                    </select>
                                </div>
                                <div class="col-md-7">
                                    <label for="">Cari Produk</label>
                                    <select class="form-control select2" id="product_name" name="product_id" onblur="this.focus()"></select>
                                </div>
                                <div class="col-md-2">
                                    <label for="">Qty</label>
                                    <input type="number" class="form-control" placeholder="1" name="qty" min="1" max="1000" id="product_qty">
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div class="row" id="loading-product-price">
                            <div class="col-12 product">
                                <div class="product-description">
                                    <div class="product-place product-text-price"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="view-product-price" style="display:none">
                            <div class="col-3">
                                <strong>Harga</strong>
                            </div>
                            <div class="col-1">
                                <strong>:</strong>
                            </div>
                            <div class="col-8 text-right">
                                <strong id="main-view-price">Rp. 0</strong>
                                {{-- <button href="#" id="removeItem" class="btn btn-primary">-</button>
                                <strong id="qtyItem">&nbsp; 0&nbsp;</strong> --}}
                            </div>
                            <div class="col-12" id="addItemFrame">
                                <button href="#" id="addItem" class="btn btn-primary">Tambahkan</button>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Detail Barang</h4>
                            <div class="card-header-action">
                                <div class="product" style="vertical-align:middle" id="loading-product-views">
                                    <div class="product-description">
                                        <div class="product-place product-text-views"></div>
                                    </div>
                                </div>
                                <span tooltip="Dilihat sebanyak" id="main-view-views" style="display:none;"><i class="fas fa-eye"></i> <strong>0</strong></span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row" id="loading-product-detail">
                                <div class="col-12 product">
                                    <div class="product-info">
                                        <center><div class="product-place product-img"></div></center>
                                    </div>
                                    <div class="product-description">
                                        <div class="product-place product-text"></div>
                                        <div class="product-place product-text"></div>
                                        <div class="product-place product-text"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="view-product-detail" style="display:none">
                                <div class="col-12 text-center mb-4">
                                    <img class="col-6 img-fluid" src="{{ asset('uploads/product/default.jpg') }}" id="main-view-picture" alt="Generic placeholder image">
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-5">
                                            <strong>Nama</strong>
                                        </div>
                                        <div class="col-1">
                                            <strong>:</strong>
                                        </div>
                                        <div class="col-5 text-right">
                                            <strong id="main-view-name">-</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <strong>Kode</strong>
                                        </div>
                                        <div class="col-1">
                                            <strong>:</strong>
                                        </div>
                                        <div class="col-5 text-right">
                                            <strong id="main-view-code">-</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <strong>Harga Jual</strong>
                                        </div>
                                        <div class="col-1">
                                            <strong>:</strong>
                                        </div>
                                        <div class="col-5 text-right">
                                            <strong id="main-view-price">-</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <strong>Harga Langganan</strong>
                                        </div>
                                        <div class="col-1">
                                            <strong>:</strong>
                                        </div>
                                        <div class="col-5 text-right">
                                            <strong id="main-view-price-subcribe">-</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <strong>Rak</strong>
                                        </div>
                                        <div class="col-1">
                                            <strong>:</strong>
                                        </div>
                                        <div class="col-5 text-right">
                                            <strong id="main-view-rak">-</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Keranjang Belanja</h4>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <h6>Total Harus Dibayar</h6>
                                <h4 id="total_price">0</h4>
                            </div>
                            <hr>
                            <div class="text-left">
                                <h6 class="text-center">List Barang</h6>
                                <table class="table" id="table-list">
                                    <thead>
                                        <tr>
                                            <th>Produk</th>
                                            <th>Qty</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2020 <div class="bullet"></div> Created By <a href="https://nauval.in/">Dawam Rajaa</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
    <script src="{{ asset('assets/modules/popper.js') }}"></script>
    <script src="{{ asset('assets/modules/tooltip.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/modules/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
    <script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/simple.money.format.js') }}"></script>

  <!-- Page Specific JS File -->
    <script>
        "use strict";
        $(document).ready(function(){
            let index = 0;
            sessionStorage.setItem('total_harga', 0)
            if(screen.width >= 768){
                $('#header-toko').hide();
                $('#addItemFrame').css('text-align', 'right')
            }else{
                $('#addItemFrame').css('text-align', 'center')
                $('.select2-container--default').css('width', '100%')
            }


            if(sessionStorage.getItem('type_price') != undefined){
                $('#type_price').val(sessionStorage.getItem('type_price'))
            }else{
                sessionStorage.setItem('type_price', 'biasa')
                $('#type_price').val(sessionStorage.getItem('type_price'))
            }
                

            // $(window).resize(function(){
            //     location.reload();
            // });

            $("#form-get-detail-product").on("submit", function(e) {
                e.preventDefault();
                resultsProduct();
            });

            setTimeout(function() {
                $("#loading-product-detail").hide();
                $("#loading-product-views").hide();
                $("#loading-product-price").hide();
                $("#view-product-detail").show();
                $("#main-view-views").show();
                $("#view-product-price").show();
            }, 5);

            function formatState (state) {
                console.log(state);
                if (!state.id) {
                return state.text;
                }
                var $state = $(
                '<span>' +state.text+'</span>'
                );
                return $state;
            };
            

            $(function(){
                $("#product_name").select2({
                    minimumInputLength: 2,
                    // templateResult: formatState, //this is for append country flag.
                    placeholder: '...',
                    cache: true,
                    ajax :{
                        url: "{{ url('api/search_product') }}",
                        type: "GET",
                        dataType: "json",
                        data: function (params) {
                            return {
                                q: $.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.data, function (item) {
                                    return {
                                        text: item.text,
                                        id: item.id,
                                        price: item.price
                                    }
                                })
                            };
                        }
                    }
                });
            });

            $('#addItem').click((e)=>{
                e.preventDefault();
                let totHarga = sessionStorage.getItem('total_harga')
                let price = (parseInt(sessionStorage.getItem('price')) * parseInt($('#product_qty').val()))
                totHarga = parseInt(totHarga) + parseInt(price);
                sessionStorage.setItem('qty',$('#product_qty').val());
                sessionStorage.setItem('total_harga', totHarga)  
                $('#total_price').html(sessionStorage.getItem('total_harga')).simpleMoneyFormat();
                
                $('#table-list > tbody').append('<tr id="list'+ index +'"><td>'+sessionStorage.getItem('product') +'</td><td class="text-center">'+ sessionStorage.getItem('qty') +'</td><td class="text-right">Rp. '+ (parseInt(sessionStorage.getItem('price')) * parseInt(sessionStorage.getItem('qty'))) +' <a  onclick="retrive('+ index+','+price+')" class="text-danger" style=" cursor: pointer; "><i class="fas fa-2x fa-times"></i></a></td></tr>');
                index += 1;
                alert('barang ditambahkan!');
            });

            async function resultsProduct(){
                var formData = $('#form-get-detail-product').serialize();
                
                $('#type_price').val($('#type_price').val())
                $.ajax({
                    url: "{{ route('get_detail_product') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#product_name").attr('disabled', 'disabled');
                        $("#loading-product-detail").show();
                        $("#loading-product-price").show();
                        $("#loading-product-views").show();
                        $("#view-product-detail").hide();
                        $("#view-product-price").hide();
                        $("#main-view-views").hide();
                    },
                    complete() {
                        $("#product_name").removeAttr('disabled', 'disabled');
                        $("#loading-product-detail").hide();
                        $("#loading-product-price").hide();
                        $("#loading-product-views").hide();
                        $("#view-product-detail").show();
                        $("#view-product-price").show();
                        $("#main-view-views").show();
                    },
                    success : function(result) {
                        if($('#type_price').val() == 'biasa'){
                            sessionStorage.setItem('price',result['price'].replace(',',''));
                            
                            let selected = '<option value="biasa" selected>Biasa</option>'
                            selected +=  '<option value="agen">Agen</option>'
                            $('#type_price').html(selected);
                            }
                        if($('#type_price').val() == 'agen'){
                            sessionStorage.setItem('price',result['price_subcribe'].replace(',',''));
                            
                            let selected = '<option value="biasa">Biasa</option>'
                            selected +=  '<option value="agen" selected>Agen</option>'
                            $('#type_price').html(selected);
                        }
                        sessionStorage.setItem('product',result['product']);
                        $('#main-view-price').html('Rp. ' + sessionStorage.getItem('price'));
                        $('#view-product-detail').html(result['detail']);
                        $('#main-view-views strong').html(result['views']);
                        $('#form-get-detail-product')[0].reset();
                        $('#product_qty').val(1);
                        
                    }
                });
            }

            $('#product_name').change(()=>{
                $('#type_price').val($('#type_price').val())
                sessionStorage.setItem('type_price', $('#type_price').val());
                resultsProduct();
            });
            $('#type_price').change(()=>{
                $('#type_price').val($('#type_price').val())
                sessionStorage.setItem('type_price', $('#type_price').val());
                location.reload()
            });
            
        });
    </script>
  
  <!-- Template JS File -->
  <script src="{{ asset('assets/js/scripts.js') }}"></script>
  <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>