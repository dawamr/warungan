<?php

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array('name' => 'admin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'karyawan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
        );

        Role::insert($roles);
    }
}
