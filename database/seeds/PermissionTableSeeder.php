<?php

use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_users = array(
            array('name' => 'permission.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'permission.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'permission.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'permission.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

            array('name' => 'role.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'role.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'role.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'role.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'role.manage', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

            array('name' => 'setting.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'setting.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'setting.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'setting.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

            array('name' => 'user.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'user.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'user.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'user.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

            array('name' => 'product_view.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product_view.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product_view.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product_view.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
        );

        $permission_products = array(
            array('name' => 'product.create', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product.view', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product.update', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'product.delete', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
        );

        foreach($permission_users as $permission){

            $insertPermissions = Permission::create($permission);
            $insertPermissions->assignRole('admin');
        }

        foreach($permission_products as $permission){

            $insertPermissions = Permission::create($permission);
            $insertPermissions->assignRole('admin');
            $insertPermissions->assignRole('karyawan');
        }
    }
}
