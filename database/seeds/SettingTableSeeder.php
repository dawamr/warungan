<?php

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = array(
            array('name' => 'app.name', 'value' => 'Minaku Scan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'app.description', 'value' => 'Aplikasi Scan Minaku', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'app.author', 'value' => 'Coffee Dev', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'app.version', 'value' => '1.0.0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'app.logo', 'value' => null, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'meta.author', 'value' => 'Coffee Dev', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('name' => 'meta.description', 'value' => 'Aplikasi Scan Minaku', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
        );

        Setting::insert($settings);
    }
}
