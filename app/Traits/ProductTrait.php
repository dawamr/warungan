<?php

namespace App\Traits;

use App\Models\ProductView;

trait ProductTrait
{
    public function productViews()
    {
        return $this->hasMany(ProductView::class);
    }
}
