<?php

namespace App\Traits;

use App\Models\Product;

trait ProductViewTrait
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
