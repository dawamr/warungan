<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    public function index()
    {
        return view('admin.role');
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        $insert = Role::create($data);

        if($insert) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan role']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan role']);
    }

    public function edit(Request $request)
    {
        $roles = Role::find($request->id);

        return response()->json(['status' => 'success', 'data' => $roles]);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $role = Role::find($request->id);

        $data = [
            'name' => $request->name,
        ];

        $update = $role->update($data);

        if($update) {
            return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah role']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah role']);
    }

    public function destroy(Request $request)
    {
        $role = Role::destroy($request->id);

        if($role) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus role']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus role']);
    }

    public function getManage(Request $request)
    {
        $permissions = Permission::all();
        $role = Role::find($request->id);
        $view = "";

        $view .= '<input type="hidden" id="manage-role-id" name="id" value="'.$role->id.'" required>';

        $view .= '<div class="row">';
        $view .= '<div class="col-12 text-center">';
        $view .= '<h4>Role '.ucfirst($role->name).'</h4>';
        $view .= '</div>';
        foreach($permissions as $permission){
            $checked = '';

            foreach($permission->roles as $role_permission){
                if($role->id == $role_permission->id){
                    $checked = 'checked';
                }
            }

            $view .= '<div class="form-group col-lg-6" style="margin:0!important;">';
            $view .= '<div class="custom-switches-stacked mt-2">';
            $view .= '<label class="custom-switch">';
            $view .= '<input type="checkbox" name="permission[]" value="'.$permission->id.'" class="custom-switch-input" '.$checked.'>';
            $view .= '<span class="custom-switch-indicator"></span>';
            $view .= '<span class="custom-switch-description">'.$permission->name.'</span>';
            $view .= '</label>';
            $view .= '</div>';
            $view .= '</div>';
        }
        $view .= '</div>';

        return $view;
    }

    public function manage(Request $request)
    {
        $permissions = $request->permission;
        $role = Role::find($request->id);

        if(empty($permissions)){
            $role->permissions()->detach();

            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah permission role '. ucfirst($role->name)]);
        }

        for($i = 0; $i < count($permissions); $i++) {
            $perms[] = Permission::find($permissions[$i]);
        }

        foreach($perms as $perm) {
            $data[] = $perm->name;
        }

        if(!empty($role)){
            $role->updatePermissions($data);
        }

        return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah permission role '. ucfirst($role->name)]);;
    }

    public function getRoles()
    {
        $roles = Role::all();

        return DataTables::of($roles)
                ->addColumn('action', function($role) {
                    $action = "";

                    if(auth()->user()->can('role.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-success' tooltip='Manage Role' data-id='{$role->id}' onclick='getManageRole(this);'><i class='fas fa-tasks'></i></a>&nbsp;";
                    if(auth()->user()->can('role.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Edit Role' data-id='{$role->id}' onclick='editRole(this);'><i class='far fa-edit'></i></a>&nbsp;";
                    if(auth()->user()->can('role.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Hapus Role' data-id='{$role->id}' onclick='deleteRole(this);'><i class='fas fa-trash'></i></a>";

                    return $action;
                })
                ->escapeColumns([])
                ->make(true);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ],$message);
    }
}
