<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PermissionController extends Controller
{
    public function index()
    {
        return view('admin.permission');
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        $insert = Permission::create($data);

        if($insert) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan permission']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan permission']);
    }

    public function edit(Request $request)
    {
        $permissions = Permission::find($request->id);

        return response()->json(['status' => 'success', 'data' => $permissions]);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $permission = Permission::find($request->id);

        $data = [
            'name' => $request->name,
        ];

        $update = $permission->update($data);

        if($update) {
            return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah permission']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah permission']);
    }

    public function destroy(Request $request)
    {
        $permission = Permission::destroy($request->id);

        if($permission) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus permission']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus permission']);
    }

    public function getPermissions()
    {
        $permissions = Permission::all();

        return DataTables::of($permissions)
                ->addColumn('action', function($permission) {
                    $action = "";

                    if(auth()->user()->can('permission.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Edit Permission' data-id='{$permission->id}' onclick='editPermission(this);'><i class='far fa-edit'></i></a>&nbsp;";
                    if(auth()->user()->can('permission.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Hapus Permission' data-id='{$permission->id}' onclick='deletePermission(this);'><i class='fas fa-trash'></i></a>";

                    return $action;
                })
                ->escapeColumns([])
                ->make(true);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ],$message);
    }
}
