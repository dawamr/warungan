<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index()
    {
        return view('admin.setting');
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $update_app_name = Setting::where('name', 'app.name')->update(['value' => $request->app_name]);
        $update_app_description = Setting::where('name', 'app.description')->update(['value' => $request->app_description]);
        $update_app_version = Setting::where('name', 'app.version')->update(['value' => $request->app_version]);
        if($request->has('app_logo')){
            Storage::delete(Setting::where('name', 'app.logo')->get()->first()->value);
            $app_logo = $request->file('app_logo')->store('uploads/logo');
            $update_app_logo = Setting::where('name', 'app.logo')->update(['value' => $app_logo]);
        }

        return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil memperbarui pengaturan']);

    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attrubute tidak boleh kosong',
            'max' => ':attrubute tidak boleh lebih dari :max',
            'mimes' => ':attrubute harus berformat :mimes',
        ];

        return Validator::make($data, [
            'app_name' => ['required', 'string', 'max:191'],
            'app_description' => ['required', 'string'],
            'app_version' => ['required'],
            'app_logo' => ['file', 'mimes:png,jpg,jpeg'],
        ], $message);
    }
}
