<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('admin.user', compact('roles'));
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ];

        $insert = User::create($data);

        $role = Role::find($request->role);

        $insert->assignRole($role->name);

        if($insert) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan user']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan user']);
    }

    public function edit(Request $request)
    {
        $user = User::find($request->id);
        $roles = User::all();

        return response()->json(['status' => 'success', 'data' => ['user' => $user, 'roles' => $roles]]);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all(), 'update');

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $user = User::find($request->id);

        $username = $user->username;
        $name = $request->name;
        $email = $user->email;
        $password = $user->password;

        if($username != $request->username) {
            $cek_username = User::where('username', '=', $request->username)->get()->count();

            if($cek_username != 0) {
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Username yang Anda masukkan sudah terdaftar']);
            }

            $username = $request->username;
        }

        if($email != $request->email) {
            $cek_email = User::where('email', '=', $request->email)->get()->count();

            if($cek_email != 0) {
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Email yang Anda masukkan sudah terdaftar']);
            }

            $email = $request->email;
        }

        if($request->password != null){
            $password = bcrypt($request->password);
        }

        $data = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        $update = $user->update($data);

        $role = Role::find($request->role);

        $user->assignRole($role->name);

        if($update) {
            return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah user']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah user']);
    }

    public function destroy(Request $request)
    {
        $user = User::destroy($request->id);

        if($user) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus user']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus user']);
    }

    public function getUsers()
    {
        $users = User::all();

        return DataTables::of($users)
                ->addColumn('role', function($user) {
                    $role = $user->roles->first()->name;

                    return $role;
                })
                ->addColumn('action', function($user) {
                    $action = "";

                    if(auth()->user()->can('user.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Manage User' data-id='{$user->id}' onclick='manageUser(this);'><i class='fas fa-tasks'></i></a>&nbsp;";
                    if(auth()->user()->can('user.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Edit User' data-id='{$user->id}' onclick='editUser(this);'><i class='far fa-edit'></i></a>&nbsp;";
                    if(auth()->user()->can('user.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Hapus User' data-id='{$user->id}' onclick='deleteUser(this);'><i class='fas fa-trash'></i></a>";

                    return $action;
                })
                ->escapeColumns([])
                ->make(true);
    }

    protected function validator(array $data, $type = 'insert')
    {
        $uniqueUsername = '';
        $uniqueEmail = '';
        $password = ['string', 'max:191', 'confirmed'];
        if($type == 'update'){
            $uniqueUsername = 'required';
            $uniqueEmail = 'required';
            $password = [];
        }

        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'unique' => ':attribute sudah terdaftar',
            'confirmed' => ':attribute tidak sama',
        ];

        return Validator::make($data, [
            'username' => ['required', 'string', 'max:191', $uniqueUsername],
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'max:191', 'email', $uniqueEmail],
            'password' => $password,
            'role' => ['required', 'string', 'max:191'],
        ],$message);
    }
}
