<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductView;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $product_count = Product::all()->count();
        $karyawan_count = User::where('roles.name', '=', 'karyawan')
                                    ->join('user_role', 'user_role.user_id', '=', 'users.id')
                                    ->join('roles', 'roles.id', '=', 'user_role.role_id')
                                    ->get()->count();
        $history_this_day_count = ProductView::where('created_at', '>', Carbon::now()->subDay())->get()->count();
        $history_this_month_count = ProductView::where('created_at', '>', Carbon::now()->subMonth())->get()->count();

        return view('admin.dashboard', compact('product_count', 'karyawan_count', 'history_this_day_count', 'history_this_month_count'));
    }
}
