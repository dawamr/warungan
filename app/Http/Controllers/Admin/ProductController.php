<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        return view('admin.product');
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $picture = null;

        if($request->has('picture')){
            $picture = $request->file('picture')->store('uploads/product');
        }

        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'price' => str_replace(',','', $request->price),
            'price_subcribe' =>str_replace(',','',  $request->price_subcribe),
            'rak' => str_replace(',','', $request->rak),
            'picture' => $picture,
        ];

        $insert = Product::create($data);

        if($insert) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan produk']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan produk']);
    }

    public function edit(Request $request)
    {
        $products = Product::find($request->id);

        return response()->json(['status' => 'success', 'data' => $products]);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all(), 'update');

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $product = Product::find($request->id);

        $picture = $product->picture;

        if($request->has('picture')){
            Storage::delete($product->picture);
            $picture = $request->file('picture')->store('uploads/product');
        }

        $data = [
            'code' => $request->code,
            'name' => $request->name,
            'price' => str_replace(',','', $request->price),
            'price_subcribe' =>str_replace(',','',  $request->price_subcribe),
            'rak' => $request->rak,
            'picture' => $picture,
        ];

        $update = $product->update($data);

        if($update) {
            return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah produk']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah produk']);
    }

    public function destroy(Request $request)
    {
        $product = Product::find($request->id);
        Storage::delete($product->picture);

        $product = Product::destroy($request->id);

        if($product) {
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus produk']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus produk']);
    }

    public function getProducts()
    {
        $products = Product::all();

        return DataTables::of($products)
                ->editColumn('price', function($product) {
                    $price = $product->price;

                    $price = number_format($price, 0, ',', '.');

                    return "Rp. " . $price;
                })
                ->editColumn('price_subcribe', function($product) {
                    $price_subcribe = $product->price_subcribe;

                    $price_subcribe = number_format($price_subcribe, 0, ',', '.');

                    return "Rp. " . $price_subcribe;
                })
                ->editColumn('rak', function($product) {
                    $rak = $product->rak;

                    return "Rak " . $rak;
                })
                ->editColumn('picture', function($product) {
                    $picture = "<img class='img-fluid' src='".asset('uploads/product/default.jpg')."' width='100px' />";

                    if($product->picture != null) $picture = "<img class='img-fluid' src='".asset($product->picture)."' width='150px' />";

                    return $picture;
                })
                ->addColumn('action', function($product) {
                    $action = "";

                    if(auth()->user()->can('product.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Edit Produk' data-id='{$product->id}' onclick='editProduct(this);'><i class='far fa-edit'></i></a>&nbsp;";
                    if(auth()->user()->can('product.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Hapus Produk' data-id='{$product->id}' onclick='deleteProduct(this);'><i class='fas fa-trash'></i></a>";

                    return $action;
                })
                ->escapeColumns([])
                ->make(true);
    }

    protected function validator(array $data, $type = 'insert')
    {
        $uniqueCode = '';
        if($type == 'insert'){
            $uniqueCode = 'unique:products,code';
        }
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'file' => ':atrribute harus berupa file',
            'mimes' => 'format :attribute harus :mimes',
            'unique' => ':attribute yang Anda masukkan sudah terdaftar'
        ];

        return Validator::make($data, [
            'code' => ['required', 'string', 'max:191', $uniqueCode],
            'name' => ['required', 'string', 'max:191'],
            'price' => ['required', 'string', 'max:191'],
            'price_subcribe' => ['required', 'string', 'max:191'],
            'rak' => ['string', 'max:191'],
            'picture' => ['file', 'mimes:png,jpg,jpeg', 'max:4096'],
        ],$message);
    }
}
