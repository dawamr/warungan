<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductViewController extends Controller
{
    public function index()
    {
        return view('admin.product_view');
    }

    public function getHistory()
    {
        $productViews = ProductView::all();

        return DataTables::of($productViews)
                ->addColumn('product_name', function($productView) {
                    $product = $productView->product;

                    return $product->name;
                })
                ->editColumn('created_at', function($productView) {
                    $created_at = Carbon::parse($productView->created_at)->diffForHumans();

                    return $created_at;
                })
                ->escapeColumns([])
                ->make(true);
    }
}
