<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        return view('global.profile');
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $user = User::find(auth()->user()->id);

        $username = $user->username;
        $name = $request->name;
        $email = $user->email;
        $password = $user->password;

        if($username != $request->username) {
            $cek_username = User::where('username', '=', $request->username)->get()->count();

            if($cek_username != 0) {
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Username yang Anda masukkan sudah terdaftar']);
            }

            $username = $request->username;
        }

        if($email != $request->email) {
            $cek_email = User::where('email', '=', $request->email)->get()->count();

            if($cek_email != 0) {
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Email yang Anda masukkan sudah terdaftar']);
            }

            $email = $request->email;
        }

        if($request->password != null){
            $password = bcrypt($request->password);
        }

        $data = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        $update = $user->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah profile Anda']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal mengubah profile Anda.']);

    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'email' => ':attribute tidak valid'
        ];

        return Validator::make($data, [
            'username' => ['required', 'string'],
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'password' => ['confirmed'],
        ], $message);
    }
}
