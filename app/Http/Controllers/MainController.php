<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductView;
use Illuminate\Http\Request;
use stdClass;

class MainController extends Controller
{
    public function index()
    {
        return view('main');
    }

    public function getDetailProduct(Request $request)
    {
        $product = Product::where('id', '=', $request->product_id)->get()->first();

        $product_count = $product ? 1 : 0;

        if($product_count == 0) {
            $detail = '<div class="col-12 text-center">';
            $detail .= '<h3>Produk tidak ditemukan</h3>';
            $detail .= '<h5>Produk yang Anda cari tidak tersedia di sistem kami</h5>';
            $detail .= '</div>';

            $price = number_format(0, 0, '.', ',');
            $product_name = '-';
            $views = 0;

            return response()->json(['status' => 'error', 'price' => $price, 'product' => $product_name, 'views' => $views, 'detail' => $detail]);
        }

        $insertView = ProductView::create(['product_id' => $product->id]);

        if(!$insertView) {
            $detail = '<div class="col-12 text-center">';
            $detail .= '<h3>Gagal Menemukan Barang</h3>';
            $detail .= '<h5>Barang yang Anda cari gagal kami temukan, Mohon lakukan sekali lagi</h5>';
            $detail .= '</div>';

            $price = number_format(0, 0, '.', ',');
            $product_name = '-';
            $views = 0;

            return response()->json(['status' => 'error', 'price' => $price, 'product' => $product_name,'views' => $views, 'detail' => $detail]);
        }

        $picture = asset('uploads/product/default.jpg');

        if($product->picture != null) {
            $picture = asset($product->picture);
        }

        $detail  = '<div class="col-12 text-center mb-4">';
        $detail .= '<img class="img-fluid col-6" style="max-height: 280px; width: auto;" src="'.$picture.'" id="main-view-picture" alt="'.$product->name.'">';
        $detail .= '</div>';
        $detail .= '<div class="col-12">';
        $detail .= '<div class="row">';
        $detail .= '<div class="col-4">';
        $detail .= '<strong>Nama</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-1">';
        $detail .= '<strong>:</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-7 text-right">';
        $detail .= '<strong id="main-view-name">'.$product->name.'</strong>';
        $detail .= '</div>';
        $detail .= '</div>';
        $detail .= '<div class="row">';
        $detail .= '<div class="col-4">';
        $detail .= '<strong>Kode</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-1">';
        $detail .= '<strong>:</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-7 text-right">';
        $detail .= '<strong id="main-view-code">'.$product->code.'</strong>';
        $detail .= '</div>';
        $detail .= '</div>';
        $detail .= '<div class="row">';
        $detail .= '<div class="col-4">';
        $detail .= '<strong>Harga Jual</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-1">';
        $detail .= '<strong>:</strong>';
        $detail .= '</div>';
        $detail .= '<div class="col-7 text-right">';
        $detail .= '<strong id="main-view-code">'.number_format($product->price).'</strong>';
        $detail .= '</div>';
        $detail .= '</div>';
        $detail .= '<div class="row">';
        $detail .=  '<div class="col-4">';
        $detail .=      '<strong>Harga Langganan</strong>';
        $detail .=  '</div>';
        $detail .=  '<div class="col-1">';
        $detail .=      '<strong>:</strong>';
        $detail .=  '</div>';
        $detail .=  '<div class="col-7 text-right">';
        $detail .=      '<strong id="main-view-code">'.number_format($product->price_subcribe).'</strong>';
        $detail .=  '</div>';
        $detail .= '</div>';
        $detail .= '<div class="row">';
        $detail .=  '<div class="col-4">';
        $detail .=      '<strong>Rak</strong>';
        $detail .=  '</div>';
        $detail .=  '<div class="col-1">';
        $detail .=      '<strong>:</strong>';
        $detail .=  '</div>';
        $detail .=  '<div class="col-7 text-right">';
        $detail .=      '<strong id="main-view-code">'.$product->rak.'</strong>';
        $detail .=  '</div>';
        $detail .= '</div>';
        $detail .= '</div>';
        $detail .= '</div>';

        $product_name = $product->name;
        $price = number_format($product->price, 0, '.', ',');
        $view = ProductView::where('product_id', $product->id)->get();
        $views = count($view);

        return response()->json(['status' => 'success', 'msg' => 'Berhasil mendapatkan barang yang Anda cari, berikut detail barangnya', 'price' => $price, 'price_subcribe' => $product->price_subcribe, 'product' => $product_name, 'rak' => $product->rak, 'views' => $views, 'detail' => $detail]);
    }

    public function SearchProduct(Request $request){

        $product = Product::where('name', 'like', '%'. $request->q.'%')->select('id', 'name as text', 'price')->paginate(5);
        return response()->json($product, 200);

    }
}
