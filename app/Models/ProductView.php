<?php

namespace App\Models;

use App\Traits\ProductViewTrait;
use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
    use ProductViewTrait;

    protected $fillable = ['product_id'];
}
