<?php

namespace App\Models;

use App\Traits\ProductViewTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ProductViewTrait;

    protected $fillable = ['code', 'name', 'price', 'price_subcribe', 'rak','picture'];
}
